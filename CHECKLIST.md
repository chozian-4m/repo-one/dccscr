Welcome to Iron Bank! Follow the below steps to onboard your container(s).

## Getting Started

- [ ] Create a Repo1 account by visiting its [login page](https://repo1.dso.mil/users/sign_in) and clicking 'Sign in with Platform One SSO,' then 'Click here to register now.' This will provide you access to the public container repository.

- [ ] Complete the [onboarding form](https://p1.dso.mil/products/iron-bank/getting-started). Iron Bank will process the request and create your GitLab repository and first Issue (whose description contains a simplified container hardening checklist). You may then proceed with the hardening process.

  - Comment on this GitLab Issue to communicate with Iron Bank staff throughout the hardening process.

- [ ] Attend our weekly onboarding session—register [here](https://www.zoomgov.com/meeting/register/vJIsce6rpzkqGq9hHHRscNfGENYqvRL1s10%E2%81%A9).

_A note on requesting tool access: during onboarding, Iron Bank asks which users will need access to the GitLab repository and [Vulnerability Assessment Tracker (VAT)](https://vat.dso.mil). Beyond this point, to request additional user access to either of these tools, open an Issue in your GitLab repository using one of the `Access Request` or `VAT User Access Request` templates, respectively._

## Repository Structure and Content

### Repository Structure [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/blob/master/Hardening/structure_requirements.md)]

- [ ] The root of the repository contains each of the following:
  - A `Dockerfile`
  - A `hardening_manifest.yaml`
  - A `LICENSE` (also known as an End User License Agreement (EULA))
  - A `README.md` with instructions for using the Iron Bank version of the image

- [ ] Configuration files are located in a `config` directory.

- [ ] If possible, the project is [configured for automatic renovate updates](https://repo1.dso.mil/dsop/dccscr/-/blob/master/Hardening/Renovate.md).

  - [ ] The root of the repository contains a `renovate.json` that includes a `reviewers` field indicating who will be notified for future Merge Requests.

### Dockerfile [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/blob/master/Hardening/Dockerfile_Requirements.md)]

- [ ] There is one Dockerfile named, simply, `Dockerfile`.

- [ ] The Dockerfile contains `BASE_REGISTRY`, `BASE_IMAGE`, and `BASE_TAG` arguments. (These are used for local builds; the container hardening pipeline will use the values of these arguments in `hardening_manifest.yaml`.)

- [ ] The Dockerfile is based on a hardened Iron Bank image.

- [ ] If it is an application container, the Dockerfile includes a `HEALTHCHECK`.

- [ ] The Dockerfile starts the container as a non-root user. (If it must be run as root, proper justification is provided.)

- [ ] Scripts referenced in the Dockerfile are located in a `scripts` directory at the project's root, including any referenced in the Dockerfile's `ENTRYPOINT`.

- [ ] The Dockerfile uses no `ADD` instructions.

### Hardening Manifest [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/tree/master/hardening%20manifest)]

Note that developers are encouraged to begin with [this example](https://repo1.dso.mil/dsop/dccscr/-/blob/master/hardening%20manifest/hardening_manifest.yaml) and update it with relevant information.

- [ ] The `hardening_manifest.yaml` adheres to the schema found [here](https://repo1.dsop.io/ironbank-tools/ironbank-pipeline/-/blob/master/schema/hardening_manifest.schema.json).

- [ ] The values for both the `BASE_IMAGE` and `BASE_TAG` arguments refer to a hardened Iron Bank image. (Note that in the pipeline, the value of `BASE_REGISTRY` defaults to `registry1.dso.mil/ironbank`.)

- [ ] The `labels` section contains values for metadata tags, for example `org.opencontainers.image.title` and `mil.dso.ironbank.image.type`, among others.

- [ ] All downloaded resources include a lowercase SHA checksum value.

- [ ] Credentials for resource URLs requiring authentication have been provided to an Iron Bank team member.

- [ ] The `maintainers` section contains contact information for container maintainers.

## GitLab CI Pipeline [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/tree/master/pipeline)]

- [ ] Validate the container builds successfully in the GitLab CI pipeline.

- [ ] Review the `check-cves` pipeline stage's scan output.

- [ ] Fix or provide justification for found vulnerabilities. Justifications should be added in [VAT](https://vat.dso.mil) (view the full VAT documentation [here](https://repo1.dso.mil/dsop/dccscr/-/blob/master/pre-approval/vat.md)). Rerun the pipeline before requesting a merge to the development branch.

_Note that justifications must be provided in a timely fashion. Prolonged delays could result in the identification of new findings, which may require this process be restarted._

## Pre-Approval [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/tree/master/pre-approval)]

- [ ] Open a Merge Request to the development branch.

- [ ] Ensure the feature branch has been merged into development.

- [ ] On the GitLab Issue, apply the `Approval` label and remove the `Doing` label. This indicates the container is ready to be reviewed by Iron Bank.

## Approval [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/tree/master/approval)]
These tasks are completed by Iron Bank, and are only included here for transparency.

- [ ] Iron Bank's Container Hardening Team conducts a peer review of the container.

- [ ] Iron Bank's Findings Approver reviews justifications.

- [ ] Iron Bank's Authorizing Official reviews the approval request and assigns the Issue one of the following labels:

  - `Approval::Expiring`: indicates this container has been granted conditional approval

  - `Approved`: indicates this container has been approved

_Note that if the container is rejected at any point during the approval process, the Issue's `Approval` label will be removed and the `Open` label added. All problems will be noted in the comments of the Issue. Once they've been addressed, you must re-add the `Approval` label._

## Post-Approval [[Full documentation](https://repo1.dso.mil/dsop/dccscr/-/tree/master/post%20approval)]

- [ ] Your development branch has been merged into master, and the GitLab Issue has been closed.

- [ ] The master branch pipeline has completed successfully. (At this point, the image is made available on `ironbank.dso.mil` and `registry1.dso.mil`.)

_Note that after your application has been approved, your container(s) will be continuously monitored. If new Common Vulnerabilities and Exposures (CVEs) are discovered or bugs identified, a new Issue will be created in your GitLab repository. As you make changes to remedy the problem(s), ensure you're adhering to all hardening requirements outlined in this guide._
