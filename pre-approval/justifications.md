# Justifications

This document provides more detailed instructions on how to justify vulnerabilities for Ironbank containers. It is intended to be used for training purposes of new Ironbank members.

#### :exclamation: **Watch the following [video](./justifications.mp4) for a short introduction to writing justifications.**

**Prerequisites:**
- A Container Hardener has downloaded their justification spreadsheet from the 'csv output' stage of their container's development-branch pipeline run.

_In this document, we are going to use_ [examples/active-mq-artemis_2.17.0-169206-justifications.xlsx](./active-mq-artemis_2.17.0-169206-justifications.xlsx) _as a training example._

_DISCLAIMER_: Container Hardeners are only responsible for providing justifications in the Twistlock Vulnerability Results, Anchore CVE Results, and Anchore Compliance Results tabs of their spreadsheet. Additionally, Container Hardeners are not responsible for justifying vulnerabilities that are listed as "Inherited from base image."

## Twistlock Vulnerability Results - Examples

In this section, we are going to provide some example justifications for the Twistlock Vulnerability Results tab and some guidance for how to research/evaluate Twistlock vulnerabilities.

**CVE-2020-13956** (row 24)

| id             | cvss | desc                                                                                                                                                                                                                           | link                                                            | packageName                           | packageVersion | severity | status                 | vecStr                                       | Justification |
| -------------- | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------- | ------------------------------------- | -------------- | -------- | ---------------------- | -------------------------------------------- | ------------- |
| CVE-2020-13956 | 5.3  | Apache HttpClient versions prior to version 4.5.13 and 5.0.3 can misinterpret malformed authority component in request URIs passed to the library as java.net.URI object and pick the wrong target host for request execution. | https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2020-13956 | org.apache.httpcomponents\_httpclient | 4.5.2          | medium   | fixed in 5.0.3, 4.5.13 | CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N |

As we can see, Twistlock has identified that httpclient-4.5.2 is vulnerable to CVE-2020-13956, and the issue was fixed in httpclient-5.0.3 and httpclient-4.5.13. We know that httpclient wasn't installed via `dnf/yum` in the Dockerfile (so Red Hat isn't relevant to the justification) and we can verify that httpclient is present in the image by searching for "httpclient" in the **SBOM java** tab (`/opt/activemq-artemis/web/console.war:WEB-INF/lib/httpclient-4.5.2.jar`). In the NVD link provided, there are several hyperlinks included, but there isn't much concrete info, so we'll need to do some additional research in order to find the date that a fixed version was released. 

If we search for "httpclient 4.5.13", we should find the [Apache HttpComponents Downloads page](https://hc.apache.org/downloads.cgi) and the [Apache httpcomponents-client GitHub releases page](https://github.com/apache/httpcomponents-client/releases). From the HttpComponents Downloads page, if we navigate to **HttpClient 4.5 > Download > HttpClient**, we'll see that 4.5.13 was last updated on 10/3/2020 and from the GitHub releases page, we'll see that 4.5.13-RC1 was published on 10/3/2020, so our justification would be:
```
Upstream has patched on 10/3/2020 in version 4.5.13. Apache ActiveMQ Artemis includes older version.
```
Essentially, we are forced to accept the risk because we are downloading the latest version of ActiveMQ Artemis, but Apache still includes the older/vulnerable version of httpclient in their release. We cannot upgrade to a non-vulnerable version of httpclient because we do not maintain ActiveMQ Artemis and there is no guarantee that 4.5.13 or 5.0.3 would work with Artemis 2.17.0.

**NOTE:** if we search for "CVE-2020-13956" in our spreadsheet, we'll notice that Anchore also identified httpclient-4.5.2 was vulnerable (Anchore CVE Results tab, row 107). Therefore, we can copy our justification over to that row as well.

## Anchore CVE Results - Examples

In this section, we are going to provide some example justifications for the Anchore CVE Results tab and some guidance for how to research/evaluate Anchore vulnerabilities.

**VULNDB-220038** (row 148)

| tag                                                                           | cve           | severity | feed   | feed\_group            | package      | package\_path                                                      | package\_type | package\_version | fix   | url                                                                                                                                                                                                                                      | inherited | description                                                                                                                                                                                                                              | nvd\_cvss\_v2\_vector      | nvd\_cvss\_v3\_vector                        | vendor\_cvss\_v2\_vector   | vendor\_cvss\_v3\_vector                     | Justification |
| ----------------------------------------------------------------------------- | ------------- | -------- | ------ | ---------------------- | ------------ | ------------------------------------------------------------------ | ------------- | ---------------- | ----- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------- | -------------------------------------------- | -------------------------- | -------------------------------------------- | ------------- |
| registry1.dsop.io/ironbank-staging/opensource/apache/active-mq-artemis:169206 | VULNDB-220038 | Critical | vulndb | vulndb:vulnerabilities | log4j-1.2.17 | /opt/activemq-artemis/web/console.war:WEB-INF/lib/log4j-1.2.17.jar | java          | 1.2.17           | 2.8.2 | \[{'source': 'CVE ID', 'url': 'http://cve.mitre.org/cgi-bin/cvename.cgi?name=2019-17571'}\] - additional links removed for formatting purposes; see examples/active-mq-artemis\_2.17.0-169206-justifications.xlsx for full list of links | no\_data  | Apache Log4j contains a flaw that is triggered as the SocketServer class accepts log data from untrusted network traffic, which it then insecurely deserializes. This may allow a remote attacker to potentially execute arbitrary code. | AV:N/AC:L/Au:N/C:P/I:P/A:P | CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H | AV:N/AC:L/Au:N/C:C/I:C/A:C | CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H |

As we can see, Anchore has identified that log4j-1.2.17 is vulnerable to VULNDB-220038, and the issue was fixed in log4j-2.8.2. We know that log4j wasn't installed via `dnf/yum` in the Dockerfile (so Red Hat isn't relevant to the justification) and we can verify that log4j is present by looking at the **package_path** column (`/opt/activemq-artemis/web/console.war:WEB-INF/lib/log4j-1.2.17.jar`). We also know that the vulnerability was not inherited from our base image because the **inherited** column DOES NOT state "True".

VulnDB findings are unique in that they typically provide a list of URLs that can refer to a CVE, bug fix, or other advisory. In this case, the VulnDB finding refers to CVE-2019-17571, as noted by the _'source': 'CVE ID'_ link in the **url** column, so we should navigate to that [MITRE page](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2019-17571) in order to find out more about the vulnerability. If we open the first link that MITRE provides, we'll see the [latest thread](https://lists.apache.org/thread.html/eea03d504b36e8f870e8321d908e1def1addda16adda04327fe7c125%40%3Cdev.logging.apache.org%3E) from the Apache Developers, who state that log4j-1.2 reached end of life in August 2015, the CVE relates to CVE-2017-5645, and users should upgrade to a supported/non-vulnerable version (ie. 2.8.2). If we search for "log4j 2.8.2", we should find the [Apache Log4j Release Change Report page](https://logging.apache.org/log4j/2.x/changes-report.html), where we can see the date of the non-vulnerable release, so our justification would be:
```
Refers to CVE-2019-17571 and CVE-2017-5645. Upstream has patched on 4/2/2017 in version 2.8.2. Apache ActiveMQ Artemis includes older version.
```
Again, we are essentially forced to accept the risk because we are downloading the latest version of ActiveMQ Artemis, but Apache still includes the older/vulnerable/EOL version of log4j in their release. We cannot upgrade to a non-vulnerable version of log4j because we do not maintain ActiveMQ Artemis and there is no guarantee that 2.8.2 would work with Artemis 2.17.0.

**NOTE:** if we search for "CVE-2019-17571" in our spreadsheet, we'll notice that Anchore also found log4j-1.2.16 was vulnerable to VULNDB-220038 (Anchore CVE Results tab, row 144) and Twistlock also found log4j-1.2.17 & log4j-1.2.16 were vulnerable to CVE-2019-17571 (Twistlock Vulnerability Results tab, rows 20 & 22). Therefore, we can copy our justification over to those rows as well.

## Anchore Compliance Results - Examples

The Anchore Compliance Results tab consists of the final STOP, WARN, and GO gate actions for each vulnerability and compliance violation that was found in the container image. These actions are configured via a DoD policy bundle and will not affect an image's entry into Registry1.

_DISCLAIMER:_ Container Hardeners are only responsible for yellow rows with the “stop” gate action. 

Ideally, justifications on the Anchore Compliance Results tab would either be "Inherited from base image." or "See Anchore CVE Results tab.", but we may occasionally see compliance violations show up as container images are being worked on. Here are a couple of the commonly encountered compliance violations and how to address them:

**Dockerfile directive ADD found**
- The use of `ADD` is prohibited. This compliance violation will be removed if you use `COPY` in your Dockerfile instead. See the [Approved Container Requirements](../Hardening/Dockerfile_Requirements.md#add-is-prohibited-use-copy).

**USER root found as effective user**
- Starting a container as `USER root` or `USER 0` is not allowed, unless the container absolutely requires it. This compliance violation will be removed if you set the last effective USER to something else in your Dockerfile. If your container requires root, you will need to provide a justification as to why the container requires root. See the [Container Execution Guidelines](../Hardening/Dockerfile_Requirements.md#container-execution-guidelines).

**Dockerfile directive HEALTHCHECK not found**
- A healthcheck is required for all Dockerfiles, unless the container is not exposing any ports or running anything. This compliance violation will be removed if you add a HEALTHCHECK in your Dockerfile. See the [Approved Container Requirements](../Hardening/Dockerfile_Requirements.md#include-a-healthcheck).

**Private certificate/key found**
- The Ironbank Security Team will not approve any containers that have private files associated with the “secret_scans” gate. This compliance violation will be removed if you explicitly remove the file and/or add `--nodocs` when installing packages. See [Remove Certificates and Keys](../Hardening/Dockerfile_Requirements.md#remove-certificates-and-keys).



_Congratulations on making it to the end of this Ironbank training document! We hope these documents help as you learn more about the Ironbank processes and procedures. If you think these documents could be further improved, feel free to open MRs into this repo, or reach out to Phillip Record (Ironbank Government Lead), Josh Eason (Ironbank PM), or Sean Melissari (Ironbank Anchor). Thank you and welcome to Ironbank!!!_
