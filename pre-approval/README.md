## Approval Preparation:  

After the container successfully builds in your working branch, complete the following:

1. Submit a Merge Request to the development branch and notate that in the issue. Someone from the container hardening team will merge it. Ensure the merge succeeds and the development pipeline runs successfully.  
Click [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) for information on creating merge requests.

> **Note**: Steps 2-4 can be managed using the Vulnerability Assessment Tracker (VAT). See the [documentation](./vat.md) for details.

2. Download the justification spreadsheet from the 'csv output' stage of the container's **development-branch** pipeline run. See [spreadsheet.md](spreadsheet.md) for instructions on how to download the scan findings spreadsheet.

    Note: The justifications must be provided in a timely fashion. Failure to do so could result in new findings being identified which may start this process over.

3. Provide justifications for any finding that is not inherited from the base image. They will be highlighted yellow on the spreadsheet. See [justifications.md](justifications.md) for instructions on how to properly provide justifications.

4. After justifications have been completed on the spreadsheet, the Container Hardener should comment and attach the justified .xlsx file to the issue. For example:
    ```
    Justifications attached.

    example-justifications.xlsx
    ```

5. Add the ~Hardening::Approval label to the issue. This will automatically add the issue to Iron Bank Security's reviewal queue.

6. Check the following checkboxes in the issue.

    Justifications:
    - [x] All findings have been justified per the above documentation
    - [x] Justifications have been attached to this issue
    - [x] Apply the label ~Hardening::Approval to indicate this container is ready for the approval phase
