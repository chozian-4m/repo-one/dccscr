# Vulnerability Assessment Tracker

The Vulnerability Assessment Tracker (VAT) provides end-to-end management of justifications for known vulnerabilities and facilitates the [Iron Bank](https://ironbank.dso.mil) approval process. The VAT aggregates findings from various container scanning tools such as Anchore, Prisma Cloud, and OpenSCAP.

## Table of Contents

- [Getting Started](#getting-started)
- [Justifications](#justifications)
- [Lifecycle](#lifecycle)
- [Notifications](#notifications)
- [API Reference](#api-reference)
- [Support](#support)

## Getting Started

The VAT is available at: [https://vat.dso.mil](https://vat.dso.mil)

To get started:
- Create an `Access Request` issue within your GitLab repository
- Note which container images you need write access to

There are three roles within the VAT:

1. **Container Contributor**: vendors and contributors
1. **Findings Reviewer**: Container Hardening Team (CHT) cybersecurity engineers
1. **Container Approver**: authorizing Officials and delegates

### Workflow

![Workflow](img/vat-workflow.png)

> Note: you must also assign the ~Hardening::Approval label on the GitLab issue ticket after submitting a container for review.

## Justifications

The following table defines the different types available for justifications.

| Justification      | Description |
| ------------------ | ----------- |
| False Positive     | False positives include items that a scanner incorrectly identifies such as a wrong package or version. This does NOT include findings that are mitigated or not vulnerable. |
| Disputed           | Issues marked as DISPUTED within the NVD. This does NOT include issues a contributor is disputing. It must be marked as such within the NVD. |
| True Positive      | Image is vulnerable to this finding. Default state of a new finding. |
| No Fix Available   | There is no patch available. This ONLY considers the vulnerable library itself, not downstream products. |
| Won't Fix          | Issues marked as WONT_FIX by the vendor. Mainly reserved for OS distributions. |
| Distro Package     | Vulnerability is for a library provided by the Operating System (OS) distribution. Only applicable when using the latest version of a distribution and library. |
| Pending Resolution | Upstream project is aware of vulnerability and is tracking an issue ticket to fix. |
| Unreleased         | Fix is available in a branch for the next release, but is not yet available. |
| Mitigated          | Issue has a mitigation that reduces severity or risk. |
| Not Vulnerable     | Issue is not exploitable within application. |

### Examples

#### False Positive

Use `False Positive` when the scanner incorrectly identifies a finding. Typical false positives include matching against the wrong architecture, language, operating system, package, or version.

* [CVE-2021-38297](https://nvd.nist.gov/vuln/detail/CVE-2021-38297) -- opensource/mattermost/mattermost:6.4.2

  > Mattermost is not compiled as a WASM module so is not affected.

* [CVE-2017-18589](https://nvd.nist.gov/vuln/detail/CVE-2017-18589) -- opensource/external-secrets/kubernetes-external-secrets:8.5.4

  > kubernetes-external-secrets uses the npm cookie library. This vulnerability is for a Rust crate.

* [CVE-2022-21673](https://nvd.nist.gov/vuln/detail/CVE-2022-21673) -- opensource/grafana/grafana:8.4.2

  > Fixed in Grafana 7.5.13 and 8.3.4. Grafana 8.4.2 or greater is installed.

* [CVE-2020-17753](https://nvd.nist.gov/vuln/detail/CVE-2020-17753) -- opensource/nodejs/nodejs12:12.22.10

  > nodejs uses the npm rc package which is a configuration loader for nodejs. This vulnerability is for rc, a command interpreter and programming language similar to sh.

* [CVE-2018-16487](https://nvd.nist.gov/vuln/detail/CVE-2018-16487) -- opensource/wordpress/wordpress:5.9.2

  > WordPress is using lodash 4.17.19 or greater and is not affected. The scanner is incorrectly matching against Underscore.js 1.8.3 in the lodash.js file.

#### NVD Disputed

* [CVE-2018-20200](https://nvd.nist.gov/vuln/detail/CVE-2018-20200) -- opensource/wildfly/wildfly:25.0.0.Final

  > Refers to a disputed CVE that Square will not fix because there is no useful action to take. https://github.com/square/okhttp/issues/4967.

#### True Positive

* [CVE-2021-36221](https://nvd.nist.gov/vuln/detail/CVE-2021-36221) -- opensource/velero/velero:v1.8.1

  > Fix available in Go 1.16.7 on 2021-08-05. velero is compiled with Go 1.17.6 or greater and is not affected. restic is using 1.16.6 and has not had a new release since 2021-08-03. restic is not tracking upgrading to Go 1.17, but its release pipeline uses the latest version of 1.16.x. https://github.com/restic/restic/blob/master/docker/Dockerfile.

#### Pending Resolution

Use `Pending Resolution` when the affected application has created an issue to resolve the finding, but has not fixed it yet. Include a reference to the issue ticket if available.

* [CVE-2021-43859](https://nvd.nist.gov/vuln/detail/CVE-2021-43859) -- opensource/keycloak/keycloak:17.0.0

  > Fix available in XStream 1.4.19 on 2022-01-29. Keycloak will upgrade to XStream 1.4.19 in their 17.0.1 and 18.0.0 release. https://github.com/keycloak/keycloak/issues/10115. XStream is a transitive dependency of Infinispan and is affected.

#### Unreleased

Use `Unreleased` when the affected application has resolved the finding, but has not released it yet. Include a reference to the pull request if available.

* [CVE-2022-23772](https://nvd.nist.gov/vuln/detail/CVE-2022-23772) -- opensource/goharbor/harbor-core:2.4.1

  > Fix available in Go 1.17.7 on 2022-02-10. harbor-core has upgraded to Go 1.17.7 on their main branch, but has not released 2.5.x yet. https://github.com/goharbor/harbor/pull/16415.

* [CVE-2021-45105](https://nvd.nist.gov/vuln/detail/CVE-2021-45105) -- opensource/apache-pulsar/pulsar:2.9.1

  > Fix available in log4j 2.17.0 on 2021-12-17. pulsar has upgraded to log4j v2.17.1 in their master branch, but has not cut a new release yet. https://github.com/apache/pulsar/pull/13552.

#### Not Vulnerable

* [CVE-2020-26160](https://nvd.nist.gov/vuln/detail/CVE-2020-26160) -- opensource/kiali/kiali:v1.47.0

  > Fix available in jwt-go 4.0.0-preview1 on 2020-01-06. Kiali has not patched, but claims to not be affected. They are tracking an upgrade to golang-jwt, but are considering it as a future upgrade instead of a bug/security fix. https://github.com/kiali/kiali/issues/4396.

* [CVE-2020-29652](https://nvd.nist.gov/vuln/detail/CVE-2020-29652) -- opensource/kaniko/kaniko:v1.8.0

  > Kaniko does not run an SSH server so is not vulnerable.

* [CVE-2019-17571](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2019-17571) -- opensource/apache/zookeeper:3.7.0

  > Zookeeper does not use SocketServer so is not vulnerable. https://issues.apache.org/jira/browse/ZOOKEEPER-3677?focusedCommentId=17010526&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-17010526. Zookeeper is tracking upgrading to logback in 3.8.x: https://issues.apache.org/jira/browse/ZOOKEEPER-4427

## Lifecycle


* **Active**: the image is under active support
* **Archived**: the image is no longer required or the contributor has withdrawn support
* **EOL**: the software is no longer maintained upstream and is end of life
* **Rejected**: the image carries too much risk for Iron Bank


## Questions

#### What is the difference between `Mitigated` and `Not Vulnerable`?

**Mitigated**: A mitigation has been applied to reduce the severity or risk of an exploit. For example:
  - Increasing the attack complexity
  - Removing attack vectors
  - Increasing the privileges required
  - Reducing the impact to confidentiality, integrity, or availability

In other words, any change that would reduce the CVSS score is considered a mitigation. This generally applies to findings where an active step has been taken such as modifying a configuration setting.

**Not Vulnerable**: A vulnerability is not exploitable within an application. This primarily applies to shipping a vulnerable library, but a path to exploit the vulnerability does not exist. For example:
  - Exploit requires parsing untrusted input, but the application does not parse untrusted input
  - Exploit requires a specific function, but the application does not use that function

For vulnerabilities that are not exploitable because of an incorrect package, operating system, architecture, language, or version, use `False Positive`. For vulnerabilities that are not exploitable because a mitigation has been applied, use `Mitigated`.

## Support

For any issues or feedback, please submit a GitLab issue in your repository.
