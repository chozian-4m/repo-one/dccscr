# Download scan findings spreadsheet

This document provides detailed instructions for accessing and downloading the scan findings spreadsheet.

**Prerequisites:**
- The container has been merged to development.
- The pipeline for the development branch has succeeded.

## Download the justifications spreadsheet

- In the repository for your container, click on "CI / CD" from the sidebar on the left.

- Locate the most recent development branch pipeline job, click on the green checkmark for the "csv-output" stage and click on "csv output".
![csv-output stage](img/1.PNG)

- In the "Job artifacts" section of the right sidebar, click "Browse".

- In the Artifacts list, click on "ci-artifacts".

- Click on "scan-results".

- Click on "csvs".

- Click on the file that ends with "-justifications.xlsx".

- Click "Download".
