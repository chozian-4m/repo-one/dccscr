# Repo Structure Requirements

This document will walk users through the process of creating and Iron Bank compliant repository structure. Failure to comply with the required structure could lead to pipeline failures and/or delays the approval process.

*Please note that this guide is a living document subject to changes and should therefore be referred to periodically when publishing container(s) to the Iron Bank to help ensure a successful build.*

*If errors are found in this guide or additional clarification is needed regarding the publication of your container(s), please document this inside your existing Gitlab issue for your application.*

**Project Structure**

At this point, your project repository has been created for you. Notice that a .gitlab directory and README.md have been created by default. The current default branch is development. Your work must take place on a feature branch (not development or master).

Required Folder Structure:

    container-project (feature-branch)
    │                             
    ├── Dockerfile                     (required)
    ├── hardening_manifest.yaml        (required)
    ├── LICENSE                        (required)
    ├── README.md                      (required)
    ├── renovate.json                  (file to be created if needed)
    ├── config/                        (directory to be created if needed)
    ├── documentation/                 (directory to be created if needed)
    ├── scripts/                       (directory to be created if needed)
    └── .gitlab/                       (created by default)

**Note: Do _not_ submit source code to the repository. Additional directories can be included if necessary.**

Required Branching Structure:

    master (the officially approved version of the application)
    ├── development (the application version being submitted for official approval)
        └── feature branches

**Note: In order for your application _and_ version to be considered approved and published in Registry1, it _must_ be merged to master. Under our current process, a member of the container hardening team will approve the merge into development. The merge into master will only be performed by the security team. It is your responsibility to ensure your application is fully completed and can be tracked via your Gitlab issue for your container.**

## Required Components

This section will detail the various components required for your container(s) to be accepted into Ironbank. This includes the various requirements for the Dockerfile, hardening_manifest.yaml, LICENSE, README, and any other files required that will be sent directly to DSOP via a Merge Request.

**Note:** If you are a contributor and you have a product that is both enterprise and open source/free software, you _must_ provide both containers to be added to Repo One. We will not accept enterprise containers if the free container isn’t also provided.

**1. Dockerfile**

Your repository should only have one Dockerfile named `Dockerfile` and must be located within the root of the project repository.

For more information on how this Dockerfile should be structured, please see [Dockerfile_Requirements.md](./Dockerfile_Requirements.md).

**2. hardening_manifest.yaml file**

Contributors will need a hardening_manifest.yaml file in the root of the project repository which provides details about the image.

For more information on the hardening_manifest.yaml file, please see [here](../hardening%20manifest/README.md)

**3. LICENSE**

The license is required for all containers and should be included in the root of the project along with the Dockerfile. If you have an opensource container, include an opensource license. If you have a container for a commercial product, please indicate in your README.md the kind of license required to run your product in addition to providing a copy of the license/EULA.

Your license file must be specifically called `LICENSE` and must be in plain text. If you are submitting a PDF version of the license, you can submit it as-is but your `LICENSE` file must specifically refer to it and both files must be included within your container.

**4. README**

A README.md is required. It should include, at minimum:

&nbsp;&nbsp;&nbsp;&nbsp;a) An overview of what the container is used for

&nbsp;&nbsp;&nbsp;&nbsp;b) Instructions on launching/running the containe. Iron Bank containers are specifically meant to run on Kubernetes for production deployments and _not_ on Docker Swarm, CLI or other environments. Any instructions for environments outside of Kubernetes will be rejected.

&nbsp;&nbsp;&nbsp;&nbsp;c) A list of recommended resource requirements for running container:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) Min/max cpu --desired state

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) Min/max memory --desired state

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) Storage min/max/limits --Allow us to scale accordingly and limit apps from requesting terra/pentabytes of storage

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4) How many storage volumes the application needs

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5) Max number of containers

&nbsp;&nbsp;&nbsp;&nbsp;d) Hyperlinks to reference documentation or other resources (e.g. upstream repository, deployment instructions)

Additional details can be specified within your README.md file as you see fit.

**5. Renovate.json**

Renovate handles updating out-of-date dependencies and can be customized for individual projects by placing `renovate.json` in the root of the repository. For more information on this process and what this file should contain, please see the [renovate documentation](./Renovate.md).

**6. Config directory**

Default configuration files for an application should reside within Repo1 in the `config` directory. This allows reviewers to easily see the contents of any configuration files. The default configuration should enable any relevant security settings.

**7. Documentation directory**

Aside from the README, any additional documentation should be placed into a `documentation` directory in the root of the project.

**8. Scripts directory**

If you have any scripts that are used in the Dockerfile (e.g. entrypoint script), you may NOT include them in a tarball pulled via the hardening_manifest.yaml file. Create a `scripts` directory at the root of the project, include them there, and copy them from the build context to the image.

Scripts **will not** have any download commands. Downloads must be done in the 
`resources` section of the`hardening_manifest.yaml` file.

Container hardening should be done within the Dockerfile whenever possible.

Examples of scripts which are permitted (by no means an exhaustive list):
- scripts which serve as an entrypoint to the container on startup
- scripts which are utilized during the container runtime and do not include any external calls to the internet
- scripts which enable the container to start or connect to a database

*DO NOT* include scripts in the `hardening_manifest.yaml` file. The container **will not** 
be approved if this is the case. You *MUST* include all scripts in this directory
via upload to the Repo1 project repository.