# Renovate

[Renovate](https://docs.renovatebot.com/) is a free and open source tool that automatically updates dependencies. Renovate supports multiple languages and platforms such as docker, golang, node, etc. See [here](https://docs.renovatebot.com/modules/datasource/) for a complete list. 

---

## How Renovate is Implemented for Iron Bank

Our renovate-bot runs daily so any vendor that wants create their own configuration of Renovate is free to do so. The following sections outline how we utilize Renovate on opensource projects that are mangaged by the Container Hardening Team to watch for updates to upstream resources in those projects. Ultimately, the vendor is responsible for updating their container in Ironbank. The use of Renovate is not currently required but is an option we provide for their use.

Renonvate has the capability to update dependencies from a wide range of sources, however, due to the nature of our container hardening process we utilize a very narrow subset of Renovate's capabilites.

The Ironbank pipeline requires all upstream resources to be declared in the hardening_manifest.yaml. Due to this we use Renovate to parse the hardening_manifest.yaml for declared versions and check for updates using the outlined datasource in the renovate.json file.

**For a more in-depth overview of Iron Bank's Renovate implementation, please see the [Renovate](https://repo1.dso.mil/dsop/container-hardening-tools/renovate/renovate) repository.**

---
**Note**

There is a limitation for this usage.  Your upstream resource must be stored in a location for which renovate has a datasoure (a full list of supported datasources can be found [here](https://docs.renovatebot.com/modules/datasource/) ).  Unfortunatly, this is a limiting factor for some projects.

It is important to remove any references to a specific application version in the filename of the hardening_manifest.yaml file when downloading artifacts that are not images. This way, if the artifact version is bumped, the COPY statement included in the Dockerfile will be able to handle this updated version, without failing because it attempts to copy an older version which is no longer being downloaded.

For example, use the filename kubernetes.tar.gz instead of the filename kubernetes-1.18.3 because the Dockerfile COPY kubernetes-1.18.3.tar.gz statement will throw an error that a resource cannot be found if the Kubernetes artifact version is bumped to 1.18.4. COPY kubernetes.tar.gz will handle any version bumps that occur and result in successful copying of the hardening_manifest.yaml artifact for the build.

---

### Hardening_manifest

Specifically, there are three pieces of data in the `hardening_manifest` that Renovate watches.

    - tags
    - labels
    - resources

*The following are example snippits from the ArgoCD project hardening_manifest that identify these:*

```yaml
tags:
- "v1.8.7"

labels:
    org.opencontainers.image.version: "v1.8.7"

resources:
- url: "docker://docker.io/argoproj/argocd@sha256:ce34acd7bac34d5a4fdbf96faf11fa5e01a7f96a27041d4472ca498886000cbf"
tag: "argoproj/argocd:v1.8.7"

```

A custom **manager** is developed for Ironbank to handle extracting dependencies and datasources from the `hardening_manifest.yaml` file resource section.

`docker`, `github-releases`, and `ruby-gems` **datasources** are supported, but other native datasources can be added as well. The manager parses the `url` key to determine the type of dependency.

```yaml
- url: docker://{registry}/{repo}@{digest}
- url: https://github.com/${repo}/releases/download/${version}/${archive}
- url: https://rubygems.org/downloads/fluentd-1.11.4.gem
```

### Regex Manager

Renovate includes a regex **manager** that can extract dependencies with a regular expression. This is useful for file formats that do not have an associated manager. Within Ironbank, the regex manager is used to handle updating the `hardening_manifest` tags and labels section.

The example `renovate.json` below will update the **tags:** and **labels:** strings if there is an update to the argocd docker image found on docker.io.

```json
    {
    "fileMatch": [
        "^hardening_manifest.yaml$"
    ],
    "matchStrings": [
        "org\\.opencontainers\\.image\\.version:\\s+\"(?<currentValue>.+?)\""
    ],
    "depNameTemplate": "argoproj/argocd",
    "datasourceTemplate": "docker"
    },
    {
    "fileMatch": [
        "^hardening_manifest.yaml$"
    ],
    "matchStrings": [
        "tags:\\s+-\\s+\"(?<currentValue>.+?)\""
    ],
    "depNameTemplate": "argoproj/argocd",
    "datasourceTemplate": "docker"
    }
```

Notice the `depNameTemplate` and `datasourceTemplate` that defines the dependency metadata.

### Major Versions

Repositories that are pinned to a major version (i.e. `dsop/opensource/redis/redis6`) should disable major revision bumps.

```json
{
    "major": {
        "enabled": false
    }
}
```

### Reviewers

Add reviewers for each repository so `MAINTAINERS` will be notified of a new pull request. Renovate can also be configured to automerge pull requests.

```json
{
    "reviewers": [
        "jeason"
    ]
}
```

---

**Note**
For more indepth documentation and configuration options you can visit the official documentation [here](https://docs.renovatebot.com/configuration-options/).

---

## Renovate Workflow

In order to begin using Renovate in your project you must create a renovate.json file in the root directory of your project so that renovate-bot will detect the file.

If an update is detected, a merge request will be created with those changes.  An Issue will also be created and associated with that merge request.  It is then required that the normal container hardening process is followed with that merge request.  The pipeline must pass, the changes merged into development, and any new CVEs must be justified or remediated before requesting it to be merged into master.