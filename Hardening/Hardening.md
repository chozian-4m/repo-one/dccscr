# Container Hardening

This document provides detailed instructions on how to build and harden container images for Ironbank. It is intended to be used for training purposes.

**Prerequisites:**
- A Container Hardener has assigned themself an issue from the "Open" column of the [Development issue board](https://repo1.dso.mil/groups/dsop/-/boards/2). See [example_issue.md](./example_issue.md) for an example issue template.
- A project for the issue has been created by a Repo One admin in the appropriate location under the [Ironbank Containers group](https://repo1.dso.mil/dsop).

_In this document, we are going to use_ [Opensource/Apache/Active-MQ-Artemis](https://repo1.dso.mil/dsop/opensource/apache/active-mq-artemis) _as a training example._

## Step 1: Locate the upstream Dockerfile

In order to rebuild and harden a containerized application on an approved base image, we must first locate the application's upstream documentation and Dockerfile. In this example, if we search for _apache activemq artemis_, we should find the official [Apache ActiveMQ Artemis homepage](https://activemq.apache.org/components/artemis/) as well as the official [Apache ActiveMQ Artemis GitHub repo](https://github.com/apache/activemq-artemis). In the GitHub repo, if we look inside the _artemis-docker_ directory, we'll see multiple Dockerfiles for different Linux distributions. Since our approved base images use Red Hat Enterprise Linux (RHEL), and RHEL is most similar to CentOS, we'll use **Dockerfile-centos** as our source of reference as we rebuild the container image on one of our approved base images.

## Step 2: Locate the required dependencies

The next step in hardening a container is to identify what the container image needs to build properly. If we inspect **Dockerfile-centos**, we'll notice that Apache builds `FROM jboss/base-jdk:8` on [line 20](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/Dockerfile-centos#L20), installs the `libaio` package on [line 35](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/Dockerfile-centos#L35), adds the `activemq-artemis` directory on [line 39](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/Dockerfile-centos#L39), and copies the `docker-run.sh` entrypoint script on [line 60](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/Dockerfile-centos#L60). These 4 components are what we need to find in order to make sure the container builds and runs as expected.

**1.) jboss/base-jdk:8 base image**

While Container Hardeners are allowed to copy over files and directories from public images (such as in [this example](https://repo1.dso.mil/dsop/opensource/istio-1.7/istioctl-1.7/-/blob/development/Dockerfile#L5)) in regards to multistage builds, the container's final base images MUST be an approved Ironbank base image. There isn't a `jboss/base-jdk:8` in Ironbank yet, but there is a `redhat/openjdk/openjdk8:1.8.0`, so we'll use that as our base image. 

**2.) libaio package**

Ironbank utilizes a secured satellite server to install packages via `dnf` and `yum`. If we get a running shell from the ubi8 base image and search for `libaio`, we will see that the package is available to install:
```
# in your local terminal/command prompt
docker login registry1.dso.mil -u <REGISTRY1_USERNAME> -p <REGISTRY1_CLI_SECRET>
docker pull registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.3
docker run -it registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.3

# inside the running ubi8 container
dnf search libaio
```
_If the package was not available from our satellite server_, we would have to find the location of the package on the internet and include it in our hardening_manifest.yaml. For example, if we searched for `libaio`, we should see the [CentOS Repositories website](https://centos.pkgs.org/), where we would find the [CentOS 8 BaseOS page](https://centos.pkgs.org/8/centos-baseos-x86_64/libaio-0.3.112-1.el8.x86_64.rpm.html) for the package (be sure to use **CentOS 8** and the **x86_64** architecture whenever possible as this is most compatible with RHEL/UBI). If we scroll down to "Download", we can see the Binary Package URL: [http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/libaio-0.3.112-1.el8.x86_64.rpm](http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/libaio-0.3.112-1.el8.x86_64.rpm), which is where we could download the package for local build-testing and is what we would need to include in our hardening_manifest.yaml for our offline pipeline builds.

**3.) activemq-artemis directory**

There are usually multiple places to find required files and directories, such as in a [DockerHub image](https://hub.docker.com/r/apache/activemq-artemis) or [GitHub release](https://github.com/apache/activemq-artemis/releases), but, in this case, we can use Apache's official [Distribution Directory](https://downloads.apache.org/) of downloads. If we look inside the `/activemq/activemq-artemis/2.17.0/` path, we'll see the most recent tarfile (**apache-artemis-2.17.0-bin.tar.gz**) and its associated SHA512 checksum (**apache-artemis-2.17.0-bin.tar.gz.sha512**). We can also grab a checksum by running ubi8 locally:
```
# inside the running ubi8 container
curl https://downloads.apache.org/activemq/activemq-artemis/2.17.0/apache-artemis-2.17.0-bin.tar.gz > apache-artemis-2.17.0-bin.tar.gz
sha256sum apache-artemis-2.17.0-bin.tar.gz
```
The checksum can be sha256 or sha512 and must be included for validation in [hardening_manifest.yaml](./hardening_manifest.yaml#L45).

**4.) docker-run.sh entrypoint script**

Any required scripts (including entrypoint scripts) must be included in your repo inside a `scripts` directory. The **docker-run.sh** entrypoint script can be found in the same directory as **Dockerfile-centos**: [https://github.com/apache/activemq-artemis/blob/master/artemis-docker/docker-run.sh](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/docker-run.sh). We'll simply place it in our repo and then copy it into our Dockerfile.

## Step 3: Reconstruct the Dockerfile

Now that we have located all of our required dependencies, it's time to reconstruct the Dockerfile. We can start by copying over [Dockerfile-centos](https://github.com/apache/activemq-artemis/blob/master/artemis-docker/Dockerfile-centos) into our own **Dockerfile**. We must then make the following changes:
1. Replace the FROM directive with an approved Ironbank base image per the Approved Container Requirements:
```
ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/openjdk/openjdk8
ARG BASE_TAG=1.8.0

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}
```
2. COPY our resources, scripts, and/or any GPG key signatures into the correct location within the image:
```
COPY activemq-artemis.tar.gz /scripts/docker-run.sh opt/
```
3. Remove insecure ENV variables (default usernames, passwords, host configuration, etc. should not be included in the image)
4. **Temporarily** set the USER to "root" so we can perform commands such as 'dnf install' (in the [openjdk8 Dockerfile](https://repo1.dso.mil/dsop/redhat/openjdk/openjdk8/-/blob/development/Dockerfile#L17), the USER is set to "1001"):
```
USER root
```
5. After installing packages, clean and remove the cache / After extracting tarfiles, remove the no-longer-needed tarfile / Combine Dockerfile directives where possible to minimize image layers:
```
RUN groupadd -g 1001 -r artemis; \
    useradd -r -u 1001 -g artemis artemis; \
    dnf upgrade -y; \
    dnf install -y --nodocs libaio; \
    dnf clean all; \
    rm -rf /var/cache/dnf; \
    mkdir -p /opt/activemq-artemis; \
    tar -zxf activemq-artemis.tar.gz --strip-components=1 -C /opt/activemq-artemis; \
    rm -f activemq-artemis.tar.gz; \
    mkdir /var/lib/artemis-instance; \
    chown -R artemis.artemis /var/lib/artemis-instance; \
    chmod +x docker-run.sh
```
6. Set the USER to "artemis" to prevent the container from starting as "root":
```
USER artemis
```
7. Remove the VOLUME directive. All Ironbank containers are meant to be run on Kubernetes (Kubernetes volume mounts should be used instead of Docker volume mounts)
8. Add a HEALTHCHECK when possible. Although end-users should use Kubernetes probes instead of Docker healthchecks, a healthcheck is required per the Approved Container Requirements:
```
HEALTHCHECK CMD curl -f localhost:8161 || exit 1
```

## Step 4: Build the image

We should now have a reconstructed Dockerfile (see [here](https://repo1.dso.mil/dsop/opensource/apache/active-mq-artemis/-/blob/development/Dockerfile)). Assuming we have activemq-artemis.tar.gz in our local workspace, we can build the image locally (be sure to delete any resources used for local testing before pushing your work to Repo One):
```
docker build -t apache-activemq-artemis-test:2.17.0 .
```

## Step 5: Test the image

After the image has built successfully, we should test the image to ensure it works as intended. Most of the time, upstream documentation provides a helm chart or Kubernetes manifests for how to deploy the image, however, in this case, Apache did not provide any Kubernetes support. We can perform a simple test of the container by verifying that it starts with the same output/logs as the upstream container:
```
# note output of upstream image
docker run apache/activemq-artemis
# test your locally-built image
docker run apache-activemq-artemis-test:2.17.0
```
At the moment, it is not required to provide Kubernetes deployment files or documentation, but it is strongly recommended. In this example, the Apache ActiveMQ Artemis Docker deployment documentation has been converted into pods and services for help with deploying on a Kubernetes cluster (see the [deployment directory](https://repo1.dso.mil/dsop/opensource/apache/active-mq-artemis/-/tree/development/deployment)). 

_DISCLAIMER_: Ironbank is not an end-to-end testing workshop. Container Hardeners are not expected to devote copious amounts of time to testing.

## Step 6: Ensure that requirements are satisfied

If your container image successfully builds and runs properly AND your repo satisfies the Approved Container Requirements (you have included a LICENSE, hardening_manifest.yaml, README.md, etc.), then you can push your code to your branch. If the Gitlab CI pipeline finished successfully (pipelines will trigger after every git push), you can open a Merge Request (MR) into `development` and ask another Ironbank member for review.

After another Ironbank member reviews and merges your MR into development, you should grab the vulnerability scan results from the 'csv_output' stage of the pipeline (pipelines will also trigger after every merged MR):
  - Ironbank Containers > Opensource > Apache > Active-MQ-Artemis > CI/CD > Pipelines > #169206 > csv output > Browse > ci-artifacts > scan-results > csvs > **active-mq-artemis_2.17.0-169206-justifications.xlsx**

You should now have all of the checkboxes in the Hardening section of your issue completed:
- [x] Hardening manifest is created and adheres to the schema (https://repo1.dsop.io/ironbank-tools/ironbank-pipeline/-/blob/master/schema/hardening_manifest.schema.json)
- [x] Container builds successfully through the Gitlab CI pipeline
- [x] Branch has been merged into `development`
- [x] Project is configured for automatic renovate updates (if possible)