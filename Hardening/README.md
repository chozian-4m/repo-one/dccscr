# Hardening

The purpose of this directory is to walk users through the process of hardening a container to be hosted in Iron Bank.

## Table of Contents

- [**Repo Structure Requirements**](./structure_requirements.md)
  - [**Dockerfile Requirements**](./dockerfile_requirements.md)
    - [Dockerfile.example1](./Dockerfile.example1)
  - [**Hardening Example**](./Hardening.md)
    - [Dockerfile.example2](./Dockerfile.example2)
    - [example_issue.md](./example_issue.md)
  - [**Renovate.json**](./Renovate.md)