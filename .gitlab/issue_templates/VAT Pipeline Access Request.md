# VAT Pipeline Access Request

## Project name

- <!-- Add project name here -->

## Requested container name

- <!-- Add container name from hardening manifest here -->

## List tags

- <!-- List tags from hardening manifest here -->

## Request Type

- [ ] This is a container name change.
- [ ] This is a new container.

## Validate (For Iron Bank staff)

- [ ] Verify container name isn't already used.
- [ ] Verify this project hasn't been authorized for any other project names.
  - [ ] If name change, verify old name's access has been revoked.
- [ ] If container name is already being used, verify there are no duplicate tags.

/label ~"Owner::Ironbank" ~Pipeline ~"Access Request::VAT Pipeline"
/assign @ironbank-notifications/onboarding @ironbank-notifications/pipelines
