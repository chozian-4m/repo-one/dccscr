# VAT User Access Request

VAT URL: <https://vat.dso.mil/>

## Members who need to be added (including yourself if applicable)

- <!-- list here -->

## Container(s) that members need to be added to

- <!-- list here with link to repos -->

## Do developers need read or write access to justifications

- [ ] Readonly
- [ ] Write

/label ~"Owner::Ironbank" ~Pipeline ~"Access Request::VAT User"
/assign @ironbank-notifications/onboarding @ironbank-notifications/pipelines
