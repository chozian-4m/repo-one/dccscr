## Post Approval

### Continuous Monitoring

Once a container is approved, the ~Approved label will be applied to this issue and it will be closed. You will be able to find your applications on http://ironbank.dsop.io and https://registry1.dsop.io.

In addition to the above, your application will now be subscribed to continuous monitoring. This means that any new findings discovered as part of this will need justifications. To satisfy this process, any new findings will trigger a new Gitlab issue in this project with the label ~"Container::New Findings". All members listed in the `maintainers` section of the `hardening_manifest.yaml` file will automatically be assigned. It is your responsibility as a Contributor or Vendor to monitor for this and provide justifications in a timely fashion. This newly created issue will have all the instructions necessary to complete the process. Failure to provide justifications could result in the revocation of the application's approval status.

### Updates

It is imperative that application updates be submitted as quickly as possible. We do not want applications to become stale. To help with this process, Ironbank recommends using a tool called [Renovate](https://github.com/renovatebot/renovate). This requires a `renovate.json` file to be placed in your project and can automate the creation of issues and merge requests. For more information on configuring your project for automatic updates with Renovate, please see the [Iron Bank Renovate documentation](../Hardening/Renovate.md).

If not using Renovate, it will be up to you as a Contributor or Vendor to keep this image up-to-date at all times. When you wish to submit an application update, you must create a new issue in this project using the `Application - Update` template and associate it with the corresponding merge request. If you submit a merge request alone, work will not proceed until a related issue is created. These issues are tracked using the label ~"Container::Update".

Additionally, it is imperative that all updates are followed through to completion. Simply submitting an application update but not following through on justifications and approvals does not suffice and  puts your application at risk of having its approval status revoked.

### Bugs

Occassionally, users may file bug reports for your application. It is your responsibility to monitor for these since they are created inside your project repository. Assignees will automatically be populated by the `members` section of the `hardening_manifest.yaml` file and will have the label `Bug`.

### Return to Pre-Approval step

Containers with new findings require stepping through the approval process again.

Updated containers with new finding require stepping through the approval process again.

Bugs require a fix and the container rebuilt and then resubmitted for approval.
