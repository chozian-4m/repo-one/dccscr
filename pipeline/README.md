# Container Hardening Pipeline

Every hardened container in Iron Bank utilizes the Container Hardening pipeline. At a high level, the pipeline is responsible for the following:
- ensure compliance with CHT project structure guidelines and check for the presence of required files.
- retrieve the resources used in the container build process and build the hardened container.
- perform scans of the container in order to evaluate for security vulnerabilities.
- publishe scan findings to the VAT (Vulerability Assessment Tracking tool) and the hardened container to Registry1.


## Pipeline Documentation:

This page provides an overview of the pipeline and includes information which may be helpful in getting started with the pipeline when onboarding. For in-depth information on the pipeline, including specific stage writeups and to view the code, please access the [IronBank Pipeline Documentation](https://repo1.dso.mil/ironbank-tools/ironbank-pipeline/-/blob/master/README.md).


## Pipeline Details:

#### Getting Started

It is first important to know which base image the hardened container will be built upon. The Container Hardening pipeline can be slightly different depending on this information. In the event the container which is being hardened is not built upon the UBI Base Image, you will need to work with a member of the Container Hardening team so a different pipeline template can be used for the project. Examples of these other types of base images include `distroless` and `scratch`. There are some components of the default pipeline setup which will not be compatible with `distroless` and `scratch` images and the pipeline will fail as a result.

The pipeline contains a linting stage which occurs early on in the running of the pipeline which enforces the correct repository structure and presence of required files. This stage is run in order to ensure compliance with Container Hardening team requirements. Please ensure that your repository structure meets the guidelines provided by the Container Hardening team and that the necessary files exist, otherwise, the Container Hardening pipeline will fail as a result.

#### Artifacts

The Container Hardening pipeline utilizes `GitLab CI/CD` artifacts in order to run stages which are dependent on previous stage resources and to provide Repo1 contributors with access to the artifacts produced by the pipeline which are relevant to them. This can include a tar file of the built image or a scan results file which contributors can use to enter justifications for any security findings, for example.

For more information regarding specific artifacts for each stage, please refer to the following documentation from the Container Hardening pipeline project: https://repo1.dso.mil/ironbank-tools/ironbank-pipeline#pipeline-artifacts.

#### Project Branches

The Container Hardening pipeline runs slightly differently depending on the hardened container project branch it is run on. The reason for this is to ensure a proper review of the repository content before the code is merged into a `development` branch or `master` branch, which publish items to production systems.

| Branch | Pipeline actions |
|--|--|
| Feature branches | Do not publish images to public Registry1 or findings results to the Iron Bank or VAT |
| `development` | Do not publish images to public Registry1 or findings results to the Iron Bank. Development branch runs will populate the VAT with findings results for approval from the Container Hardening security team and/or Iron Bank container approvers |
| `master` | Publish images to public Registry1 and findings results to the Iron Bank. Do not publish any findings results to the VAT |

#### Current Approved Base Images:

- [Red Hat UBI7](https://repo1.dso.mil/dsop/redhat/ubi/ubi7)
- [Red Hat UBI8](https://repo1.dso.mil/dsop/redhat/ubi/ubi8)
- [Distroless](https://repo1.dso.mil/dsop/google/distroless/base)
- [Scratch](https://repo1.dso.mil/dsop/docker/scratch)

### Container Hardening Pipeline Stages

![pipeline-overview](img/PipelineOverview.png)

#### Preprocess / Preflight / Lint

These preliminary stages set up the workspace and work together to enforce the presence of required files and directories in the repository. The formatting of various project files (Dockerfile, hardening_manifest.yaml, etc.) is also validated.

#### Import Artifacts

This stage will import any external resources (resources from the internet) provided in the hardening_maifest.yaml file for use during the container build. It will download the external resources and validate that the checksums calculated upon download match the checksums provided in the hardening_manifest.yaml file.

#### Container Build

The container build process takes place in an offline environment. The only resources that the build environment has access to are the resources included in the `hardening_manifest.yaml`, images from Registry1, and Red Hat RHEL RPMs. Any attempts to reach out to the internet will be rejected by the cluster's egress policy and the `build` stage will fail.

For more insight into the `build` stage, please refer to the following documentation from the Container Hardening pipeline project: https://repo1.dso.mil/ironbank-tools/ironbank-pipeline/-/blob/master/stages/build/README.md.

#### Scanning

There are two types of scans performed during the Container Hardening pipeline. The first scan which occurs is an anti-virus/anti-malware scan which ensures the artifacts which are used during the container build do not contain any sort of malicious files. The second type of scan which occurs in the pipeline checks for security vulnerabilities and compliance issues within the container. 

##### Anti-Virus/Anti-Malware Scanning

The anti-virus/anti-malware scanning is performed by the open source tool [ClamAV](https://www.clamav.net/). ClamAV has a database of viruses and malware which is updated daily. The scan is performed on the items which are brought into the hardening manifest file from external sources. In the event that ClamAV flags one of these artifacts as containing a virus or malware, the pipeline automatically fails. This step is performed to ensure that the items being utilized during the container build are safe to use.

##### Security Vulnerability and Compliance Scanning

This scan is enabled to ensure any vulnerabilities are accounted for (such as dependency security flags). The scan results are then generated so that an auditor can manually review potential security flags that were raised in the scan. Should the auditor determine the scan results satisfactory, publication of the container(s) to the Iron Bank will occur.

The pipeline begins by pulling the container image from Repo One, and proceeds to run a series of scans on the container:

**OpenSCAP Compliance**

The OpenSCAP Compliance scan enables us to provide configuration scans on container images.

The particular security profiles that we use to scan container images conforms to the DISA STIG for Red Hat Enterprise Linux 7 and 8. In the event the container is not compliant with the OpenSCAP security profile, the pipeline will fail this job.

**OpenSCAP CVE**

The OpenSCAP CVE scan checks for a list of security vulnerabilities to verify that containers are not vulnerable to any currently known security flaw.

Please visit [OpenSCAP](https://www.open-scap.org/) for more information on either of the previous scans.

**Twistlock**

The Twistlock scan checks container images for both vulnerabilities and compliance issues.

Please visit [Twistlock](https://www.twistlock.com/) for more information.

**Anchore**

Please contact [Anchore](https://info.anchore.com/contact) directly for an enterprise license or more information.  You can visit the [Anchore documentation](https://docs.anchore.com/current/docs/) to get started and learn more.

After these scans have completed, the pipeline takes an output of the scan findings, compiles them into a CSV file, and stores them to be accessed through the Iron Bank.  If there is a whitelist associated with the container, the pipeline checks the scans against it. This concludes the Scanning portion of the pipeline.

#### CSV Output

The csv-output stage will generate CSV files for the various scans and the Justifications spreadsheet. These documents can be found in the artifacts for this stage. The Justifications spreadsheet is created so that Container Hardening team members and vendors/contributors can access the list of findings for the container they are working on. It is a compilation of the findings generated from the scanning stages and is used as the document for submitting justifications.

#### Check CVEs

This stage prevents the publishing of images which do not have whitelisted vulnerabilities. It retrieves the whitelist for the image and will verify that the scan results do not contain any findings which have not been justified/whitelisted. This prevents the image from being published to the Iron Bank website or the Harbor registry with security vulnerabilities we are not aware of.

#### Post-Scan Steps

As mentioned [above](#project-branches), some stages of the pipeline will only run on development and master branches.

**Documentation**

This stage provides validation of the container to end users by signing the image and container manifest with with the Iron Bank public GPG key.

**Publish**

On development and master branches, this stage will upload artifacts which are displayed/utilized by the Iron Bank website to S3 (e.g. scan reports, project README, project LICENSE). Additionally, on the master branch, this stage will push built images to `registry1.dso.mil/ironbank`.

**VAT**

The VAT stage will only run on development branches. It uses pipeline artifacts from the scanning stages to populate the Vulnerability Assessment Tracker (VAT) at `vat.dso.mil`, which is limited to container contributors, findings approvers, and container approvers.

