# Frequently Asked Questions

## Is there a process for requesting applications that are not currently supported? Who is responsible for doing the work?
Yes, you can submit a request for an application on the [P1](https://p1.dso.mil/#/products/iron-bank/getting-started) website.  Ironbank will review the request and decide if the application will be maintained by the core team. Otherwise you will be responsible for contributing and updating the image.

## How long will my images be waiting for approval?
Reducing our backlog by streamlining the approval process is our number one priority. We are currently working on this and plan to release the new process late Q2/early Q3.

## Am I responsible for inherited findings?
Only if you are the owner of the parent image, otherwise no. The CHT will address any inherited findings in our parent images.

## There are many different severity levels in our findings, which ones am I responsible for?
While Medium and Low will warn, Critical and High will always fail the build. The latter two are of highest priority.

## My container is stuck in AO approval for over a month, meanwhile chore findings accrue, what do I do?
If the chore findings are remediated in the image pending AO approval, then leave the issue untouched. If you close it, then our automation will reopen the issue.

## How do I pull a container from the registry?
1. Create a Repo1 account (https://repo1.dso.mil/users/sign_in) to get access to the public repository of containers. You can register by clicking on the 'Sign in with Platform One SSO' button in the sign-in page, followed by the Register button
2. Login:
```shell
docker login registry1.dso.mil
docker pull <image>
```

### Any plans to support Windows containers (.NET framework)?
This has not been prioritized on our roadmap.

## Why is my build failing on UBI8 findings?
Are you using the latest UBI8 image in your Dockerfile?
https://registry1.dso.mil/harbor/projects/3/repositories/redhat%2Fubi%2Fubi8

`docker pull registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.5`

## How do I understand ticket priorities?
https://repo1.dso.mil/dsop/dccscr#ticket-priorities-and-what-to-expect