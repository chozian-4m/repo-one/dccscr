## Container Approval Process:

1) Ensure [all steps in the Pre-Approval Process](../pre-approval) are complete 
2) A peer review is conducted by the Container Hardening Approval Team (QA check)
3) A Findings Approver reviews and approves all justifications (Local review of provided justifications)
4) A final review may be conducted by a Container Hardening Team member before the container is sent to the Authorizing Official
5) An approval request is sent to Authorizing Official via email
6) Once an approval response is received from the Authorizing Official, the approval ticket will be updated according to the response


#### Note: If the above approval process is kicked back for any reason, the ~Hardening::Approval label will be removed and the issue will be sent back to Open. Any comments will be listed in this issue for you to address. Once they have been addressed, you may re-add the ~Hardening::Approval label.

### Step 1:

The container contributor/owner has performed all of the steps in the Approval Preparation Process, to include adding the **Approval** label to a ticket. This places the ticket in the Container Hardening Approval Team's queue. This allows a team member to pick up the ticket and shepherd the container through the approval process.

### Step 2:

The Container Hardening Approval Team member conducts a quality assurance check on the container and repo to ensure all guidelines and standards defined by Iron Bank are being met, such as having an adequate description in the README.md file.

### Step 3:

The Container Hardening Approval Team member reviews the justifications provided in the supplied container's justification spreadsheet. Each justification is reviewed for accuracy.

### Step 4:

The Container Hardening Approval Team member may send the container to another team member to review and verify the approval review results.

### Step 5:

The Container Hardening Approval Team member sends an email to the Authorizing Offical with the results of the approval review. The Authorizing Official reviews the information and makes a determination. The determination may be Authorized (The container may be used), Conditionally Approved (The container is approved for a limited amount of time, and identified items will need to be resolved in that timeframe), and Not Approved (The container has not been approved for use).

### Step 6:

The Container Hardening Approval Team member updates the ticket according to the response from the Authorizing Official. If the container is Approved, the team member merges the Development branch to the Master branch and closes the ticket. If the container is Conditionally Approved, the team member merges the Development branch to the Master branch and updates the ticket with the expiration date and the Approval::Expiring label. If the container is Not Approved, the team member posts a comment in the ticket with the Authorizing Official's response and pushes the ticket back to the Open queue.


## Labels

A number of labels are applied to a ticket as a container moves through the Approval Process. The definition of each label is described below.

### Approval process label definitions:

~Hardening::Approval - The container contributor/owner is ready for the container to go through the approval process and at this point is handing the work off to the Container Hardening Approval Team.

**Note:  The following labels should only be applied by the Container Hardening Approval Team!** 

~"Approval::Local Security" - A Findings Approver has picked up the ticket and is working through the approvals process

~"Approval::Local Review" - The Findings Approver has finished the initial steps in the approval process and is now sending the container for a final review prior to sending on to the Authorizing Official

~"Approval::Official Review" - The container has been sent to the Authorizing Official for a determination

~Approved - The Authorizing Official has fully approved the container

~"Approval::Expiring" - The Authorizing Official has conditionally approved the container
