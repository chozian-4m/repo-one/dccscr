# Overview

Iron Bank is the DoD's source for hardened and approved containers. Hardened containers do not have a CtF (certificate to field). A hardened container allows the application/container to run on an ATO'd Kubernetes cluster that meets the DevSecOps Reference Design ([see documentation](https://software.af.mil/dsop/documents/)). To get an ATO a container must go through the normal process in the downstream environment as set up in that program. Our systems assist in that process by producing a secure by design baseline that other programs can leverage and compliance and vulnerability findings and assessments that can also be leveraged. In addition, many compliance findings and vulnerabilities are resolved through the Iron Bank processes. 

To better understand Iron Bank, please refer to this list of definitions:
- Iron Bank:
  - The overarching name given to the entire effort.
  - It is also the name of the website [https://ironbank.dso.mil](https://ironbank.dso.mil). The website lists all approved containers, provides access to download artifacts such as scanning reports.
- Repo1: This git repository powering the entire container hardening effort. [https://repo1.dso.mil](https://repo1.dso.mil).
- Registry1: A full-fledged OCI-compliant registry where users can download images via command-line. [https://registry1.dso.mil](https://registry1.dso.mil)

## Key Things to Know
- **Iron Bank and Repo1 (Gitlab) are completely open to the public (available at IL2). There are no locked down versions of Iron Bank**
- Although the Iron Bank ecosystem contains a functional docker registry, direct uploads of ”external” container images to the registry is not supported.  All images pushed to the registry must be hardened and approved.
- The Iron Bank platform does not run containers. Approval of container images does not imply an “automatic” deployment to any specific environment (i.e., Party Bus).
- There are no current built in features supporting license keys, or methods to protect intellectual property (we encourage vendors to incorporate the necessary technologies to fulfil individual licensing and protection requirements).
- The Iron Bank is not a completely %100 automated platform.  Portions of the “hardening” process are manual and involve substantial human effort.


## Who is this for?

Iron Bank ultimately is for anyone to consume or contribute. However, we specifically target the following personas:
- DoD organizations wishing to consume approved containers
- DoD organizations wishing to help contribute to containers (e.g. bug fixes, new applications, updates)
- DoD Authorization Officials wishing to understand the risks associated with applications
- Commercial vendors wishing to bring their application to the DoD

## Our Methodology

Containers must follow a rigorous set of processes and requirements in order to receive an approval. Technical requirements and details can be found in this Contributor Onboarding Guide. However, at a high level, applications must follow the following requirements:
- [Rebasing the container onto an approved base image (Red Hat UBI, distroless, etc)](./Hardening/Dockerfile_Requirements.md#requirements)
- Internet disconnected build processes
- The application and all containers must be supported by a vendor, open source community, or government entity
- You must be working with the latest of the release series for your dependencies and application
- Timely justifications of all findings from the following scanners:
  - OpenSCAP: DISA STIG compliance
  - Twistlock: CVE identification
  - Anchore: CVE and DoD compliance identification
- Continuous monitoring (currently every 12 hours) and timely submission of justifications for any new findings
- Submission of any new application update(s) no later than the day of public release

This secure by design baseline makes for a consistent and more advanced starting point for which an approval from the organization's Authorization Official can then be sought by a particular program. If any portion of the above list is not satisfied, the Iron Bank approval for the application will be subject to revocation.

The goal of the process is to enable continuous approval of applications down to their specific versions. It also enables the DoD to standardize an approval process that all Authorization Official's can agree to without subjecting applications to varying degrees of approval criteria, timeframes, etc. Additionally, the approval process encompasses multiple DoD Impact Levels (IL) meaning that once an application is approved, it is approved at all Impact Levels unless specifically stated otherwise.

As alluded to above, approved applications may have restrictions placed on them. Typical restrictions can include:
- Cannot be internet facing
- Approved at specific Impact Levels
- Conditionally approved until some criteria has been met

Typically the above are based on security findings that are borderline acceptable. A roadmap to correction is required to maintain approval. If security findings are deemed to be unacceptable, the approval request will be denied with details stating it's rejection. This does not mean the application will never be approved, just that work needs to be completed to bring the security findings into an acceptable threshold.

## Contributors

Anyone wishing to contribute an application to Iron Bank must first onboard with the Container Hardening Team (the instructions are below). This includes DoD organizations, vendors, individuals, etc. This is a requirement to ensure everyone understands the appropriate processes and is assigned a Point of Contact (POC). A POC is necessary because users will have restricted permissions when contributing to projects. Specifically, `master` and `development` branches must be protected and can only be merged to by a member of the Container Hardening team. This ensures our security posture when hardening and approving images. The POC's are also there to answer any questions you have in a timely fashion via Gitlab Issues.

*Note: If you are a commercial vendor wishing to bring your product to the DoD, we ask that you have a government sponsor requesting your product in order to be properly prioritized. The more government customers requesting your product, the higher in priority your application will become. This is necessary in order to properly prioritize our backlog.*

***To fully onboard to Iron Bank, [here is a checklist to get through the whole process](./CHECKLIST.md) Below are the details for the 8 steps you will have to complete to get your containers hardened and approved on the Iron Bank. This documentation will walk you through the whole process and provide details on what to do in each step.***

***Watch the following [video](./pre-approval/justifications.mp4) for an introduction to writing justifications.***

### Getting Started

Create a Repo1 account (https://repo1.dso.mil/users/sign_in) to get access to the public repository of containers. You can register by clicking on the 'Sign in with Platform One SSO' button in the sign-in page, followed by the Register button

1. Submit your onboarding form here: https://p1.dso.mil/#/products/iron-bank/getting-started. Your repo and issue inside of Gitlab will be created for you! 

Once your repository has been created, an email notification will be sent to the requester with a link to the repository, your assigned priority, and registration information for the Onboarding meetings and Get Unstuck/AMA sessions.

2. [Hardening Process](./Hardening/README.md)

3. [Dockerfile Requirements](./Hardening/Dockerfile_Requirements.md)

4. [Hardening Manifest](./hardening%20manifest/README.md)

5. [Gitlab CI Pipeline](./pipeline/README.md)

6. [Pre-Approval](./pre-approval/README.md)

7. [Approval Process](./approval/README.md)

8. [Post-Approval](./post%20approval/README.md)

### Communication

All communication should occur through your Gitlab issue. This ensures that all information is documented in a centralized location and also ensures that all of the assignees are notified of updates. It is imperative that all required parties are listed as assignees of this issue and that individuals are not removed. Please do not remove anyone from the assignee list.

If you need to contact the Container Hardening team, please identify your assigned point of contact. You can find your point of contact by:

1. They should be listed as an assignee on this ticket

2. They should be listed in the hardening_manifest.yaml file under the maintainers section with a field of cht_member: true

If you have no assignee, feel free to tag Container Hardening leadership in your issue by commenting on this issue with your questions/concerns and then add `/cc @ironbank-notifications/leadership`. Gitlab will automatically notify all Container Hardening leadership to look at this issue and respond.

**Iron Bank Get Unstuck/AMA Working Sessions every Wednesday from 1630-1730EST.**

Need some help with your containers getting through Iron Bank? Have questions on where things are at? Are you feeling stuck and want to figure out the next steps? This is the meeting for you! Come meet with the Iron Bank leadership and engineers to get answers to your questions.

Register in advance for this meeting: https://www.zoomgov.com/meeting/register/vJIsf-ytpz8qHSN_JW8Hl9Qf0AZZXSCSmfo 

After registering, you will receive a confirmation email containing information about joining the meeting.

### Where/How do I check on the status of my container?

You should have access to [Repo1](https://repo1.dso.mil/dsop) and be checking on the status of your containers here.

For more information on checking the status of your container, please see the [Issue Board Tracking](./Issue%20Board%20Tracking.md) document.

## Consumers

Anyone wishing to consume approved images can start immediately assuming they have self-registered for any of the Iron Bank applications. You can register by clicking on the 'Sign in with **Platform One SSO**' button in the [sign-in page](https://repo1.dso.mil/users/sign_in?redirect_to_referer=yes), followed by the `Register` button.

### Primary method for identifying applications and associated data

Consumers may use [Iron Bank website](https://ironbank.dso.mil) to browse containers, retrieve scan results, etc. You may also download a TAR file of the image if desired.

### Registry access

Additionally, users can use [Registry1](https://registry1.dso.mil) if they prefer browsing containers through the official Harbor registry or downloading via command-line interfaces. However, the Harbor registry website lacks any associated data with applications and thus should be accompanied with the Iron Bank website listed above. All applications in the Registry1 project entitled `Ironbank` are officially approved images.

*Note: Downloading from the CLI requires the user to first log into Harbor registry via the website interface then retrieve their username and CLI secret by clicking on their name in the upper right corner. Provide these credentials to your CLI tool for proper access.*

# Issue Tracking

## Issue Types
Please follow the description template when making an issue or request.
There is a template for each of the following types:
- Bug
- Container Request
- Feature Request
- Support
- Documentation (issues with current or request new)

For more information on Iron Bank issue tracking, please see the [Issue Board Tracking](./Issue%20Board%20Tracking.md) document.

# Vulnerability Assessment Tracker (VAT)
[This video](https://repo1.dso.mil/dsop/dccscr/-/blob/VAT-video/Videos/vat-voiceover-take-1.mp4) provides a walkthrough of the VAT tool.

# Ticket Priorities and What to Expect
| Priority | Description | Use Cases |
| -------- | ----------- | --------- | 
| 0 | Tickets requested to be prioritized by the P1 Leadership team | When leadership needs to reprioritize the workload to meet special circumstances. Includes maintenance and updates for containers |
| 1 | Platform One Core Container - Impacts BB + PB + IB | |
| 2 | Core items for Iron Bank Pipelines | Any tools that are used as part of the DevSecOps Pipeline for Iron Bank. Includes maintenance and updates for containers |
| 3 | Core Package Items for Big Bang Pipelines | Any tools that are used as part of the DevSecOps Pipeline for Big Bang. Includes maintenance and updates for containers |
| 4 | This priority is used for any tools, products or items that are used as part of the Platform One core infrastructure (Party Bus, CNAP, Cyber, Customer Success) | Includes maintenance and updates for containers |
| 5 | This priority is used for paying customers of Platform One. Money must be already sent to Platform One for this priority to be applied. Tier ONE on the True Up initiative = A customer paying > $4M | Includes maintenance and updates for containers | 
| 6 | Tier TWO on the True Up initiative = A customer paying between  < $3.9M > $500K | Includes maintenance and updates for containers |
| 7 | Tier THREE on the True Up initiative =A customer paying < $499K | Includes maintenance and updates for containers |
| 8 | Open Source containers that are used by the community and supported by Iron Bank for the utility of Platform One Services | Includes maintenance and updates for containers |
| 9 | This priority is used for projects where there is a specific DoD/Govt customer identified and the vendor needs to get through Iron Bank before the gov customer can use the service. Potential Platform One customers who are working on payment or are interested in paying Platform One. | Mission Apps that are not paying Platform One. Includes maintenance and updates for containers |
| 10 | This is for vendor's containers who do not have a DoD/Govt customer identified but want to get their containers approved for use on Iron Bank. | Vendors who want to eventually sell into Govt. Includes maintenance and updates for containers |
| 11 | This is for open source containers that do not have a DoD/Govt customer identified and no defined user base. | Includes maintenance and updates for containers |
