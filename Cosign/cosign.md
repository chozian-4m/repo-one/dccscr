# Cosign Overview
[Sigstore Cosign](https://github.com/sigstore/cosign) is an opensource tool that is used to create cryptographic [signature artifacts](https://github.com/sigstore/cosign/blob/main/specs/SIGNATURE_SPEC.md) and store them in OCI-compliant registries alongside the artifacts that they sign.  Using Cosign to sign images in registires allows registry users and NPEs to verify the authenticity of images pulled from that repository using PKI.

### Cosign and IronBank
IronBank has implemented Cosign image signing as part of the IronBank Container Hardening pipeline, which means that all images that are pushed to registry1.dso.mil can be verfied using the [`cosign` CLI](https://github.com/sigstore/cosign/releases/tag/v1.6.0).

# Image Signature Verification
This section details how to manually verify images in registry1.dso.mil using `cosign` and IronBank PKI

### Prerequisites
There are a few prerequisites for manually verifying registry1.dso.mil image signatures:
- You have [installed the `cosign` CLI](https://github.com/sigstore/cosign#installation)
- You have downloaded the [certificate](https://repo1.dso.mil/ironbank-tools/ironbank-pipeline/-/blob/master/scripts/cosign/cosign-certificate.pem)
  - You may retrieve the certificate and place it in your current working directory by running `curl -o cosign-certificate.pem https://repo1.dso.mil/ironbank-tools/ironbank-pipeline/-/raw/master/scripts/cosign/cosign-certificate.pem`
- You have pull permissions for the `registry1.dso.mil/ironbank/` image you wish to verify

### Verification
Once these prerequsites are satisfied run the following command, replacing `path/to/example/image:tag` with the path of the image you wish to verify the signature of:
```bash
cosign verify --cert cosign-certificate.pem registry1.dso.mil/ironbank/path/to/example/image:tag
```

The output should appear similar to the following:
```bash
> cosign verify --cert cosign-certificate.pem registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.5

Verification for registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.5 --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key

[{"critical":{"identity":{"docker-reference":"registry1.dso.mil/ironbank/redhat/ubi/ubi8"},"image":{"docker-manifest-digest":"sha256:a73163c8e0a871cf6d078c81ad90ecf84b55023b71a85386fd7af1d9e0a995a1"},"type":"cosign container image signature"},"optional":{"Subject":"ironbank@dsop.io"}}]
```

**NOTE: If the `cosign verify` command outputs something else similar to the following text, this means the signature could not be verified**
```bash
> cosign verify --cert cosign-certificate.pem registry1.dso.mil/ironbank/redhat/ubi/ubi8:8.4
Error: no matching signatures:

main.go:46: error during command execution: no matching signatures:
```
