# Gitlab Issue Board
## Board views
- [Development board](https://repo1.dso.mil/groups/dsop/-/boards/2)
- [Approval board](https://repo1.dso.mil/groups/dsop/-/boards/1484?&label_name[]=Container%3A%3AApproval)

### Development board
This board spans all issues across the entire DSOP group.

Displays all work in progress, but minimizes the approval processes which are primarily used by the security team. However, any container in the Container::Approval column still indicates the approval status based on the labels in that column. So visibility is still maintained but board complexity is reduced.

Stages represented in this view:
- Open
    - Acts as a backlog for all issues.
    - Issues in this stage are not considered ready for work by CHT, but can be assigned to `To Do` or `Doing` by the vendor/contributor as necessary.
- To Do
    - All issues ready to be worked by the CHT team.
    - PM is responsible for moving issues from `Open` to `To Do` unless a vendor/contributor has done so already.
- Blocked
    - Any issue that has been blocked more than `3 days` or `72 hours`. If an issue is simply awaiting response from a vendor/contributor, it is to remain in that stage until the previously mentioned threshold has been met. Issues in this stage will be routinely evaluated to determine if blockage still exists.
- Doing
  - Any container hardening steps being conducted.
  - Issues will remain in this stage until justifications are submitted to local security members.
- Container::Approval (once an issue has moved into this stage, only security personnel will move the ticket)
  - Approval::Expiring - Used by security members to identify containers whose approval is expiring. Due dates will be a required field.
  - Approval::Local Security - Represents issues whose justifications have been submitted to local security members. Do not reassign the issue to security members. Leave all assignees as-is.
  - Approval::Local Review - Represents issues who have passed the local security stage and are pending review from CHT leadership prior to sending for final approval to the authorizing official.
  - Approval::Official Review - Issues that have been submitted for official review by the authorizing official.
- Approved
  - The container(s) are now available on the Iron Bank front end: https://ironbank.dso.mil/ironbank/repomap. Any details or restrictions on which Impact Level (IL) this container can be used at will be in the README, with a description of why. The Air Force Chief Software Office will approve the containers up to IL2, IL4, IL6 for DoD Wide use.
- Closed
  - Issues that have successfully went through all stages above.
  - Any issue that has been closed by leadership for other reasons.

Additional labels that can be applied, but do not represent a stage unto themselves:
- Application Useability
  - Bug - Represents a bug that has been discovered within the container.
  - Feature - Represents an enhancement to a container.
- Application Status
  - Container::Initial - Represents a container that is being worked for the first time.
  - Container::Update - Represents a container that is already approved but needs to be updated.
  - Container::Archive - Represents a container that is end-of-life or otherwise not supported.
  - Container::New Findings - Represents an approved container that, due to continuous monitoring, has new findings that must be justified.
- Priority
  - Prioity::High
  - Priority::Medium
  - Priority::Low
- Question:
  - Question::General - An issue that has a general question not otherwise captured elsewhere.
  - Question::Leadership - An issue that contains question for Iron Bank leadership.
  - Question::Onboarding - An issue that contains questions for the onboarding team.
- Onboarding - Represents an issue where vendors/contributors need to be onboarded.
- Pipeline - Represents an issue where the pipeline is having some issue completing successfully.
- Ownership
  - Owner::Iron Bank - All work is owned by members of the Container Hardening Team within Iron Bank
  - Owner::Contributor - All work is owned by an external contributor, an external entity who is a partner with Platform One is managing the containerization and updates to this product. 
  - Owner::Vendor - A Vendor is managing the containerization and updates to this product. All work is owned by the vendor with the exception of the approval process.


Ad-hoc labels: These are used for various reasons described below:
  - Notification::first - An Automated label to notify of inactivity - applied after 14 days of no update to the issue
  - Notification::second - automated labels to notify of inactivity - applied after 28 days of no update to the issue
  - Notification::final- automated labels to notify of inactivity on the container - applied after 42 days of no update to the issue, then the ticket gets moved to Iron Bank leadership to see if we can adjudicate the reason no work is happening.  



The label should now say its approved! and any details or restrictions on which IL level this container can be used at will be in the ReadMe File, with a description of why. The Air Force Chief Software Office will approve the containers up to IL2, IL4, IL6 for DoD Wide use. 




### Approval board
This board spans all issues across the entire DSOP group but is filtered by containers with the label `Container::Approval`.

All approval status' is displayed in an expanded view. No issues are displayed that are not in the approval phase.

### Workflows

#### Initial workflows
The diagram below shows a typical workflow when no projects exist. This is used for vendors/contributors who wish to onboard as well as consumers who would like to request applications/projects.

Note, this is a high-level diagram and some areas are glossed over. It is only meant to provide basic understanding. Any questions can be directed to the Iron Bank Container Hardening Team.

```mermaid
graph TD
    A(Customer) --> B(Vendor/Contributor)
    A --> C(Consumer)
    B -->|Submit Onboarding Ticket| D{Onboarding}
    C -->|Submit Application Request Ticket| G{Request}
    D --> H{{Teleconference/Documentation/Processes}}
    D --> I{{Create repos and tickets}}
    H & I --> M(Close ticket)
    G --> N{{Evaluate request}}
    G --> O{{Onboard vendor if necessary}}
    N & O --> P{Create project}
    P --> Q{{Create repos and tickets}}
    Q --> S(Close ticket)
```

At the end of each workflow, you see the ticket has been closed. This does not indicate work has been completed. Only this step has been completed. All work transitions to a new set of tickets that exist inside each of the project repositories. When closing the initial tickets as seen above, links will be provided in the comments of that issue to all newly created issues. This allows you as the user, to see how your work has transitioned into new projects and the status of each.

#### Project workflows
The diagram below shows a nominal workflow for all issues within a project repository. These tickets represent the actual status of projects/containers being hardened. Applications that span multiple projects/containers are not tracked via an overarching ticket, instead they are grouped by the projects created inside Gitlab itself. For example, if an application is named `Application A` and consists of `Container 1`, `Container 2`, and `Container 3` then you can track the status of this application specificailly by navigating directly to the group level inside Gitlab called `Application A` vice the individual projects.

Note that any issue post `Doing` stage can be moved back to `Doing` at any time if rejected any subsequent stage.

```mermaid
graph TD
  A[Open] -->|PM Prioritization or Vendor/Contributor Assigned| B(To Do)
  B --> C[Doing]
  A -->|Criteria not met| D[Blocked]

  C --> F[Awaiting Response]
  F -->|>72 hours| D[Blocked]
  C -->|Justifications Submitted| G[Container::Approval]
  G --> H(Approval::Local Security)
  H --> I(Approval::Local Review)
  I --> J(Approval::Final Approval)

  J --> K[Closed]
  ```