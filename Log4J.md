# Log4j CVE-2021-44228 and CVE-2021-45046

Iron Bank is reviewing and updating containers to resolve CVE-2021-44228 and CVE-2021-45046. By using our image scanning tools (Anchore and Prisma Cloud Compute) we have been able to identify instances of the vulnerable library in many containers. We are actively working to update containers to address this issue.

Until updates are available for all containers, additional mitigation is recommenced. Setting the `log4j2.formatMsgNoLookups=true` system property or `LOG4J_FORMAT_MSG_NO_LOOKUPS=true` environment variables may provide _partial_ mitigation but can be bypassed in some non-standard configurations (see CVE-2021-45046 for further information). [JVM hotpatching](https://github.com/corretto/hotpatch-for-apache-log4j2/) can also be used as a temporary mitigation (also available as a [Kubernetes DaeomonSet](https://github.com/aws-samples/kubernetes-log4j-cve-2021-44228-node-agent)).

For more information on the vulnerability see:  
* https://logging.apache.org/log4j/2.x/security.html  
* https://www.lunasec.io/docs/blog/log4j-zero-day/

Specific products may have vendor advisories available with further information.

#### Updated Iron Bank containers
The following Iron Bank containers have updated log4j to 2.15.0 or applied mitigations to address CVE-2021-44228. These containers may still be vulnerable to CVE-2021-45046, review of this new CVE is still in progress. Additional Iron Bank containers are being reviewed for vulnerability and may receive updates.

```
registry1.dso.mil/ironbank/elastic/elasticsearch/elasticsearch:7.16.1
registry1.dso.mil/ironbank/elastic/kibana/kibana:7.16.1
registry1.dso.mil/ironbank/elastic/logstash/logstash:7.16.1
registry1.dso.mil/ironbank/indrasoft/vauban/vauban:1.0.5
registry1.dso.mil/ironbank/microfocus/fortify/sca:21.2.2.0001
registry1.dso.mil/ironbank/microfocus/fortify/ssc:21.2.0-log4j-mitigated
registry1.dso.mil/ironbank/opensource/apache/nifi:1.15.1
registry1.dso.mil/ironbank/opensource/apache/nifi-registry:1.15.2
registry1.dso.mil/ironbank/opensource/neo4j/neo4j:4.4.1
registry1.dso.mil/ironbank/opensource/owasp-zap/owasp-zap:v2.11.1
registry1.dso.mil/ironbank/bdp/rda/bootstrap:2.0.0
registry1.dso.mil/ironbank/bitnami/kafka:3.0.0
registry1.dso.mil/ironbank/bitnami/zookeeper:3.7.0
registry1.dso.mil/ironbank/synopsys/coverity/coverity:2021.06
registry1.dos.mil/ironbank/synopsys/coverity/coverity-analysis:2021.12.0
registry1.dso.mil/ironbank/opensource/strimzi/operator:0.27.0
registry1.dso.mil/ironbank/opensource/keycloak/keycloak:16.1.0
regisrty1.dso.mil/ironbank/bigbang/fluentd-aggregator:1.14.2
registry1.dso.mil/ironbank/opensource/solr/solr-8:8.11.1
```

#### Iron Bank containers being reviewed for log4j impact

The following Repo1 issue searches may be used to track additional containers that may be affected by log4j. This list may not be complete, and may have false positives.

* https://repo1.dso.mil/groups/dsop/-/issues?scope=all&state=all&search=CVE-2021-44228
* https://repo1.dso.mil/groups/dsop/-/issues?scope=all&state=all&search=CVE-2021-45046


#### Vendor Advisories
* https://community.sonarsource.com/t/sonarqube-sonarcloud-and-the-log4j-vulnerability/54721
* https://confluence.atlassian.com/kb/faq-for-cve-2021-44228-1103069406.html
* https://confluence.atlassian.com/security/multiple-products-security-advisory-log4j-vulnerable-to-remote-code-execution-cve-2021-44228-1103069934.html
* https://discuss.elastic.co/t/apache-log4j2-remote-code-execution-rce-vulnerability-cve-2021-44228-esa-2021-31/291476
* https://github.com/keycloak/keycloak/discussions/9078
* https://help.sonatype.com/docs/important-announcements/sonatype-product-log4j-vulnerability-status
* https://neo4j.com/release-notes/database/neo4j-4-4-1/
* https://www.zaproxy.org/docs/desktop/releases/2.11.1/
* https://community.sonarsource.com/t/sonarqube-sonarcloud-and-the-log4j-vulnerability/54721
